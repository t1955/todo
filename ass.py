from flask import Flask, request
from flask import render_template
from flask import redirect
from flask.helpers import flash
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///todo.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.secret_key='Naresh'
db = SQLAlchemy(app)

class Todo(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    content=db.Column(db.String)
    complete=db.Column(db.Boolean ,default=False)

db.create_all()

@app.route("/")
def Home():
    incomplete=Todo.query.filter_by(complete=False).all()
    complete=Todo.query.filter_by(complete=True).all()
    return render_template("list.html",incomplete=incomplete,complete=complete)


@app.route("/add" , methods=["POST"])
def add():
    task=Todo(content=request.form['content'],complete=False)
    if not task.content:
        flash('Please enter a valid name!!!')
        return redirect("/")
         
    db.session.add(task)
    flash('you added an item successfully')
    db.session.commit()
    return redirect("/")


@app.route("/complete/<int:id>")
def complete(id):
    task=Todo.query.filter_by(id=int(id)).first()
    task.complete=True
    flash('you completed successfully')
    db.session.commit()
    return redirect("/")

    



if __name__ == '__main__':
    app.run(debug=True)